/**
 * AUTO DOWNLOAD FILE FOR VUEJS MIXIN
 * This method is useful to convert Js Object to JSON and auto download file generated,
 * like configuration file.
 * Take two arguments the first one is mandatory, it's the JS object to convert.
 * The second argument is the the name of the file. The default is configuration.json
 * @param {Object, String}
 * @returns {boolean}
 */

export default {
  methods: {
    autoDownloadFile: (objectToDownload, filename = 'data.vapi') => {
      const dataURI = encodeURIComponent(JSON.stringify(objectToDownload));

      const element = document.createElement('a');
      element.setAttribute('href', `data:text/json;charset=utf-8,${dataURI}`);
      element.setAttribute('download', filename);
      element.style.display = 'none';
      document.body.appendChild(element);
      element.click();
      document.body.removeChild(element);

      return true;
    },
  },
};
