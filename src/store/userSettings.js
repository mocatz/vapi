import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const INITIAL_STATE = {
  beautifyResponse: false,
  displayOnlyKey: '',
  responseAreaSize: 5,
  displayUserSettings: false,
};

export default {
  state: () => ({ ...INITIAL_STATE }),
  getters: {
    beautifyResponse: (state) => state.beautifyResponse,
    displayOnlyKey: (state) => state.displayOnlyKey,
    responseAreaSize: (state) => state.responseAreaSize,
    displayUserSettings: (state) => state.displayUserSettings,
  },
  mutations: {
    UPDATE_BEAUTIFY_RESPONSE(state, isBeautify) {
      state.beautifyResponse = isBeautify;
    },
    UPDATE_DISPLAY_ONLY_KEY(state, key) {
      state.displayOnlyKey = key;
    },
    UPDATE_RESPONSE_AREA_SIZE(state, value) {
      state.responseAreaSize = value;
      const r = document.querySelector(':root');
      r.style.setProperty('--response-area-size', `${value}vh`);
    },
    SWITCH_USER_SETTINGS(state) {
      state.displayUserSettings = !state.displayUserSettings;
    },
  },
  actions: {
  },
};
