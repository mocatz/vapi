import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const INITIAL_STATE = {
  routeCollection: [],
  defaultRoute: {
    routeVerb: 'get',
    routePath: 'people/2/',
    routeBody: '{\n}',
    apiResponse: '',
  },
  filter: {
    routeVerb: [],
    status: [],
  },
};

export default {
  state: () => ({ ...INITIAL_STATE }),
  // --------------------------------------------------------------------------------
  //                                   GUETTERS
  // --------------------------------------------------------------------------------
  getters: {
    routeCollection: (state) => state.routeCollection,

    routeData: (state) => (id) => state.routeCollection.find((r) => r.id === id),

    routeDataStatus: (state) => (id) => {
      const idx = state.routeCollection.findIndex((r) => r.id === id);
      return state.routeCollection[idx]?.apiResponse?.status || 0;
    },

    defaultRoute: (state) => state.defaultRoute,

    filter: (state) => state.filter,

    isFilterOn: (state) => {
      let isOn = false;
      Object.keys(state.filter).forEach((k) => {
        if (state.filter[k].length > 0) {
          isOn = true;
        }
      });
      return isOn;
    },
  },
  // --------------------------------------------------------------------------------
  //                                   MUTATIONS
  // --------------------------------------------------------------------------------
  mutations: {
    ADD_ROUTE(state) {
      state.routeCollection.push({ ...state.defaultRoute, id: new Date().getTime() });
    },

    UPDATE_ROUTE(state, data) {
      const idx = state.routeCollection.findIndex((r) => r.id === data.id);
      const payload = state.routeCollection[idx];
      payload.routeVerb = data.routeVerb;
      payload.routePath = data.routePath;
      payload.routeBody = data.routeBody;
      Vue.set(state.routeCollection, idx, payload);
    },

    DELETE_ROUTE(state, id) {
      const idx = state.routeCollection.findIndex((r) => r.id === id);
      state.routeCollection.splice(idx, 1);
    },

    UPDATE_ROUTE_VERB(state, data) {
      const idx = state.routeCollection.findIndex((r) => r.id === data.id);
      const payload = state.routeCollection[idx];
      payload.routeVerb = data.routeVerb;
      Vue.set(state.routeCollection, idx, payload);
    },

    UPDATE_ROUTE_PATH(state, data) {
      const idx = state.routeCollection.findIndex((r) => r.id === data.id);
      const payload = state.routeCollection[idx];
      payload.routePath = data.routePath;
      Vue.set(state.routeCollection, idx, payload);
    },

    UPDATE_ROUTE_BODY(state, data) {
      const idx = state.routeCollection.findIndex((r) => r.id === data.id);
      const payload = state.routeCollection[idx];
      payload.routeBody = data.routeBody === ''
        ? '{\n}'
        : data.routeBody;
      Vue.set(state.routeCollection, idx, payload);
    },

    UPDATE_ROUTE_RESPONSE(state, data) {
      const idx = state.routeCollection.findIndex((r) => r.id === data.id);
      const payload = state.routeCollection[idx];
      payload.apiResponse = data.apiResponse;
      Vue.set(state.routeCollection, idx, payload);
    },

    SET_COLLECTION_FROM_FILE(state, data) {
      state.routeCollection = data;
    },

    CLEAN_RESPONSE(state, id) {
      const idx = state.routeCollection.findIndex((r) => r.id === id);
      const payload = state.routeCollection[idx];
      payload.apiResponse = '';
      Vue.set(state.routeCollection, idx, payload);
    },

    ADD_FILTER(state, filter) {
      const [k, v] = filter;
      if (state.filter[k].includes(v)) {
        const idx = state.filter[k].findIndex((elm) => elm === v);
        state.filter[k].splice(idx, 1);
      } else {
        state.filter[k].push(v);
      }
    },

    REINIT_FILTER(state) {
      state.filter.routeVerb = [];
      state.filter.status = [];
    },
  },
  // --------------------------------------------------------------------------------
  //                                   ACTIONS
  // --------------------------------------------------------------------------------
  actions: {
    ADD_ROUTE({ commit }) {
      commit('ADD_ROUTE');
    },

    UPDATE_ROUTE({ commit }, data) {
      commit('UPDATE_ROUTE', data);
    },

    DELETE_ROUTE({ commit }, id) {
      commit('DELETE_ROUTE', id);
    },

    SET_COLLECTION_FROM_FILE({ commit }, data) {
      commit('SET_COLLECTION_FROM_FILE', data);
    },

    CLEAN_RESPONSE({ commit }, id) {
      commit('CLEAN_RESPONSE', id);
    },

    ADD_FILTER({ commit }, filter) {
      commit('ADD_FILTER', filter);
    },

    REINIT_FILTER({ commit }) {
      commit('REINIT_FILTER');
    },
  },
};
