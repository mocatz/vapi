import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import routeCollection from './routeCollection';
import apiMain from './apiMain';
import userSettings from './userSettings';

Vue.use(Vuex);

export default new Vuex.Store({
  plugins: [createPersistedState({ key: 'vapi' })],
  state: {
    deferredPrompt: null,
  },
  getters: {
    deferredPrompt: (state) => state.deferredPrompt,
  },
  mutations: {
    UPDATE_DEFERRED_PROMPT(state, e) {
      state.deferredPrompt = e;
    },
  },
  actions: {
    UPDATE_DEFERRED_PROMPT({ commit }, e) {
      commit('UPDATE_DEFERRED_PROMPT', e);
    },
  },
  modules: {
    apiMain,
    routeCollection,
    userSettings,
  },
});
