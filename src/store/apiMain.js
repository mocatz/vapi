import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const INITIAL_STATE = {
  apiPath: 'https://swapi.dev/api/',
  customHeader: '{ "authorization": "Bearer Your_JWT_here" }',
  defaultApiPath: 'https://swapi.dev/api/',
  defaultCustomHeader: '{ "authorization": "Bearer Your_JWT_here" }',
};

export default {
  state: () => ({ ...INITIAL_STATE }),
  getters: {
    apiPath: (state) => state.apiPath,
    customHeader: (state) => state.customHeader,
  },
  mutations: {
    UPDATE_API_PATH(state, apiPath) {
      state.apiPath = apiPath;
    },
    UPDATE_CUSTOM_HEADER(state, customHeader) {
      const header = customHeader === ''
        ? '{\n}'
        : customHeader;
      state.customHeader = header;
    },
    REINIT_API_PATH(state) {
      state.apiPath = state.defaultApiPath;
    },
    REINIT_HEADERS(state) {
      state.customHeader = state.defaultCustomHeader;
    },
    SET_API_FROM_FILE(state, data) {
      state.apiPath = data.apiPath;
      state.customHeader = data.customHeader;
    },
  },
  actions: {
    REINIT_API_PATH({ commit }) {
      commit('REINIT_API_PATH');
    },
    REINIT_HEADERS({ commit }) {
      commit('REINIT_HEADERS');
    },
    SET_API_FROM_FILE({ commit }, data) {
      commit('SET_API_FROM_FILE', data);
    },
  },
};
