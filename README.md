# vapi

## ABOUT

Access to the app here: [https://vapi.netlify.app/](https://vapi.netlify.app/)
VAPI (VAπ) is a pwa so you can install it on both desktop and mobile (chrome is recommended).     
The app is made with VueJS & deployed on Netlify

![desktop][desktop]
![desktop light][desktop2]
<img src="./doc_media/mobile.png" alt="drawing" width="200"/>

[desktop]: ./doc_media/desktop.png "Add your image"
[desktop2]: ./doc_media/desktop2.png "Add your image"


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
