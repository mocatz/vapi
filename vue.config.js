module.exports = {
  pwa: {
    name: 'VAPI',
    themeColor: '#1b2631',
    msTileColor: '#1b2631',
    display: 'fullscreen',
    manifestOptions: {
      background_color: '#1b2631',
    },
    workboxOptions: {
      skipWaiting: true,
      exclude: ['_redirects'],
    },
  },
};
